/* 
Non indented lines starting with ; are comments on how to modify this script, any comments at the end of lines are for documentation purposes
*/
#SingleInstance force

^!E::								; Bind to Ctrl + Alt + E. For further information check: http://www.autohotkey.com/docs/Hotkeys.htm
	WinGet, active_id, ID, A				; Remember current Window
	WinGetPos, X, Y, Width, Height, A			; Get current Window size
	oCB := ClipboardAll					; Save Clipboard
	Clipboard = 						; Empty Clipboard
	Send, ^c						; Copy currently selected text
	ClipWait, 1						; Wait for Clipboard to fill
	nCB := Clipboard					; Save selected Text from Clipboard
	ClipBoard := oCB  					; Restore old Clipboard content
	FileDelete, %TEMP%\uniedit_buffer.txt			; Delete any content of the temp file
	FileAppend, %nCB%, %TEMP%\uniedit_buffer.txt		; Write selected Text to temp file

; Change the line below to the path of your prefered editor plus any arguments it might need before the file path
	Run, C:\Program Files (x86)\Notepad++\notepad++.exe %TEMP%\uniedit_buffer.txt, , , editor_id
; e.g.: Run, C:\Program Files (x86)\Vim\vim74\gvim.exe -o %TEMP%\uniedit_buffer.txt, , , editor_id

	WinWait, ahk_pid %editor_id%, ,1			; We give the editor one second to become active, that might be not enough
								; Problem is, the editor_id might not actually be the editor but only the launched executable
								; We therefore assume the currently active window is the editor if WinWait times out
	if ErrorLevel {
		ActivateWindow:
		WinGet, editor_id, PID, A
		if % editor_id = "" {
			sleep 100
			goto, ActivateWindow
		}
	}

	WinActivate						; Activate Editor Window, implicitly makes the id known for following commands
; If you dont want your editor to change its position on launch commment out the line below
	WinMove A,, X, Y, Width, Height				; Move Editor Window to the position of the original window
	WinWaitClose, ahk_pid %editor_id% 			; Wait for the Editor to close
	WinActivate, ahk_id %active_id%				; Activate original Window
	FileRead, newContent, %TEMP%\uniedit_buffer.txt 	; Read Content from temp file
	oCB := ClipboardAll					; Save current Clipboard content
	Clipboard = %newContent%				; Put content from temp file into the clipboard
	Send, ^v						; Paste Clipboard into active window
	Clipboard = oCB						; Restore Clipboard content
	active_id = ""						; Clear temp variable
	editor_id = ""						; Clear temp variable
	return

; You can change this hotkey to the save hotkey in your editor (example Ctrl + S). For further information check: http://www.autohotkey.com/docs/Hotkeys.htm
; Do not remove the ~ though!
~^s::								; Bind to Ctrl + S. For further information check: http://www.autohotkey.com/docs/Hotkeys.htm
	WinGet, currently_active, PID, A			; Remember the current Window
	if % editor_id != currently_active {			 ; If the hotkey was not pressed in our editor it is meaningless for the script
		return
	}
								; The following lines are the same as the ones below WinWaitClose above
	WinActivate, ahk_id %active_id%				; Activate original Window
	FileRead, newContent, %TEMP%\uniedit_buffer.txt 	; Read Content from temp file
	oCB := ClipboardAll					; Save current Clipboard content
	Clipboard = %newContent%				; Put content from temp file into the clipboard
	Send, ^v						; Paste Clipboard into active window
	Clipboard = oCB						; Restore Clipboard content
	active_id = ""						; Clear temp variable
	editor_id = ""						; Clear temp variable

	Reload							; Reloading the script to cancel the WinWaitClose from above
	return
