**What it does**

UniversalEdit aims to allow you to edit any text inside your favorite editor. The idea is similar to "[It's All Text!](https://addons.mozilla.org/en-US/firefox/addon/its-all-text/)" or the discontinued Quickcursor project, but the platform is Windows.

After further research we discovered [Text Editor Anywhere](http://www.listary.com/text-editor-anywhere). For now we suggest using this as it is superior in pretty much every way.

**How to Install**

- Download [AutoHotkey](http://ahkscript.org/)
- Modify the .ahk file to reference your editor of choice
- Compile the .ahk file
- Execute the resulting .exe as administrator

**How to use**

- Mark any editable text
- Launch your editor with Ctrl + Alt + E (or your own specified hotkeys)
- You can now edit the marked text inside your prefered editor
- When you save & close your editor, the previously marked text is overwritten by whatever you wrote in your editor

**Current limitations**

- The copying works by emulating Ctrl + C and the pasting by emulating Ctrl + P respectively. If the program you are copying text from does not support these shortcuts, the script won't work
- The script does not notice if you change the initial text selection, it will paste over anything that is selected in the initial window when the editor is closed

**Proposed features**

- Allow for easy editor selection (wizard style perhabs)
- Disable access to the initial window in order to avoid unexpected behaviour